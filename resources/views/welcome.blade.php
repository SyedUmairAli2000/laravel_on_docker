<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Scripts -->
       <script src="{{ asset('js/app.js') }}" defer></script>
   
   <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
         
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">LaraApp</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link">Contact</a>
      </li>
     
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    <div class="container">
    <div class="jumbotron mt-5">
  <h1 class="display-4">Hello, world!</h1>
  <p class="lead">This simple laravel app is deployed on docker with kubernetes and gitlab CI/CD.</p>
  <hr class="my-4">
  <a class="btn btn-dark btn-lg" href="#" role="button">Learn more</a>
</div>
    </div>   
      
    </body>
</html>
